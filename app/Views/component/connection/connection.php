<div id="connection-box" class="d-flex justify-content-center align-items-center">

  <div id="connection-bg" class="container m-0 p-0 d-flex justify-content-center">
    <div class="row justify-content-center align-items-center">

      <form method="post" class="card p-0 col-sm-12 col-md-8 text-center">
        <h5 class="card-header">Se connecter</h5>
        <div class="card-body fluid-container">
          
          <div class="input-group mb-3">
            <span class="input-group-text col-2 col-sm-2">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
              </svg>
            </span>
            <input type="text" name="user" class="form-control" placeholder="utilisateur" >
          </div>
          <div class="input-group mb-3">
            <span class="input-group-text col-2 col-sm-2">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lock-fill" viewBox="0 0 16 16">
                <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
              </svg>
            </span>
            <input type="text" name="password" class="form-control" placeholder="mot de passe" >
          </div>
          <button type="submit" formaction="/home" class="btn btn-success">Me connecter</button>
        </div>
      </form>

    </div>
  
  </div>
</div>
