
<div id="recipe-box" class="d-flex justify-content-center align-items-center">

  <div id="recipe-bg" class="container m-0 p-0 d-flex justify-content-center">
    <div class="row justify-content-center align-items-center flex-column">
        
        <div id="recipe-card" class="card" style="width: 18rem;">
            <div id="recipe-title" class="card-header text-center"></div>
        </div>
        
        <div id="recipe-card" class="card" style="width: 18rem;">
            <!-- <div id="recipe-title" class="card-header text-center">
            </div> -->
            <ul id="recipe-list" class="list-group list-group-flush">
            </ul>
        </div>
      
    </div>
  </div>

</div>


