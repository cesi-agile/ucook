
<div id="ask-box" class="d-flex justify-content-center align-items-center">

  <div id="ask-bg" class="container m-0 p-0 d-flex justify-content-center">
    <div class="row justify-content-center align-items-center">

      <form method="post" class="card p-0 col-sm-12 col-md-8 text-center">
        <h5 class="card-header">Temps de préparation</h5>
        <div class="card-body fluid-container">
        
        <div class="custom-control custom-radio">
            <input type="radio" id="rapide" name="time" class="custom-control-input" value="10">
            <label class="custom-control-label" for="customRadio1">Pas plus de 10 minutes!</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="moyen" name="time" class="custom-control-input" value="30">
            <label class="custom-control-label" for="customRadio2">J'ai 30 à 40 minutes devant moi!</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="long" name="time" class="custom-control-input" value="60">
            <label class="custom-control-label" for="customRadio2">J'ai le temps de laisser mijoter le plat!</label>
        </div>
        <button type="submit" formaction="/ingredient" class="btn btn-success">Étape suivant</button>
      </form> 
    </div>
  </div>
</div>
