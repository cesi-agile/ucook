<div id="ask-box" class="d-flex justify-content-center align-items-center">
  <div id="ask-bg" class="container m-0 p-0 d-flex justify-content-center">
    <fdiv class="row justify-content-center align-items-center">
      <form method="post" class="card p-0 col-sm-12 col-md-8 text-center">
        <h5 class="card-header">Mes contraintes</h5>
        <div class="card-body fluid-container"> 
        <div class="row">
          <div class="form-check col-6 icon">
            <input class="form-check-input" type="checkbox" name="produit[]" id="tomate" value="tomate" multiple>
            <label class="form-check-label" for="flexRadioDefault1">
              tomate
            </label>
          </div>
          <div class="form-check col-6 icon">
            <input class="form-check-input" type="checkbox" name="produit[]" id="jambon" value="jambon" multiple>
            <label class="form-check-label" for="flexRadioDefault2">
              jambon
            </label>
          </div>
        </div>
        <div class="row">
          <div class="form-check col-6 icon">
            <input class="form-check-input" type="checkbox" name="produit[]" id="patate" value="patate" multiple>
            <label class="form-check-label" for="flexRadioDefault1">
              pomme de terres
            </label>
          </div>
          <div class="form-check col-6 icon">
            <input class="form-check-input" type="checkbox" name="produit[]" id="oignon" value="oignon" multiple>
            <label class="form-check-label" for="flexRadioDefault2">
              oignon
            </label>
          </div>
          <div class="form-check col-6 icon">
            <input class="form-check-input" type="checkbox" name="produit[]" id="emmental" value="emmental" multiple>
            <label class="form-check-label" for="flexRadioDefault3">
              emmental
            </label>
          </div>
        </div>
          <button type="submit" formaction="/recipe" class="btn btn-success">Propose moi une recette !</button>
        </div>
      </form>  
</div>
  </div>
</div>
