
<div id="ask-box" class="d-flex justify-content-center align-items-center">

  <div id="ask-bg" class="container m-0 p-0 d-flex justify-content-center">
    <div class="row justify-content-center align-items-center">

      <form method="post" class="card p-0 col-sm-12 col-md-8 text-center">
        <h5 class="card-header">Votre niveau en cuisine</h5>
        <div class="card-body fluid-container">
        
        <div class="custom-control custom-radio">
            <input type="radio" id="debutant" name="level" class="custom-control-input" value="debutant">
            <label class="custom-control-label" for="customRadio1">Débutant</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="intermediaire" name="level" class="custom-control-input" value="intermediare">
            <label class="custom-control-label" for="customRadio2">Intermédiaire</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="expert" name="level" class="custom-control-input" value="expert">
            <label class="custom-control-label" for="customRadio2">Expert</label>
        </div>
        <button type="submit" formaction="/time" class="btn btn-success">Étape suivant</button>
      </form> 
    </div>
  </div>
</div>


