<?php

namespace App\Controllers;

class ConnectionController extends BaseController
{
    private $session;
    private $productsModel;

    public function __construct(){
        $this->session = \Config\Services::session();
        $this->productsModel = model(ProductsModel::class);
    }
    
    public function index()
    {
        $params = $this->request->getPost();
        
        $filters['userSettings'] = $this->session->get('filterSettings');
        
        $data = [
            'title' => "UCook",
            'css' => [
                        'templates/header.css',
                        'connection/connection.css',
            ],
            'js' => [
                        'component/connection.js',
            ]
        ];
        
        return  view('template/header', $data).
                view('component/general/banner').
                view('component/connection/connection').
                view('template/footer');
    }
}
