<?php

namespace App\Controllers;

use App\Models\AskModel;

class AskController extends BaseController
{
    private $session;
    private $askModel;

    public function __construct(){
        $this->session = \Config\Services::session();
        $this->askModel = model(AskModel::class);
    }
    
    public function recipe(){
        $params = $this->request->getPost();
        if(isset($params['produit'])){
            $this->session->set('produit', $params['produit']);
        }

        $data = [
            'title' => "UCook",
            'settings' => $_SESSION,
            'css' => [
                        'templates/header.css',
                        'ask/ask.css',
            ],
            'js' => [
                        'component/ask.js',
            ]
        ];  
        
        return  view('template/header', $data).
                view('component/general/banner').
                view('component/ask/recipe').
                view('component/ask/variables').
                view('template/footer');
        
        // var_dump($_SESSION);
    }

    public function level()
    {
        /* $params = $this->request->getPost();
        if(isset($params['level'])){
            $this->session->set('level', $params['level']);
        } */
        
        // $this->askModel->callOpenIA();
        
        $data = [
            'title' => "UCook",
            'level' => $this->session->get('level'),
            'css' => [
                        'templates/header.css',
                        'ask/ask.css',
            ],
            'js' => [
                        'component/fake.js',
            ]
        ];  
        
        return  view('template/header', $data).
                view('component/general/banner').
                view('component/ask/level').
                view('template/footer');

    }

    public function time()
    {
        $params = $this->request->getPost();
        if(isset($params['level'])){
            $this->session->set('level', $params['level']);
        }
        
        // $this->askModel->callOpenIA();
        
        $data = [
            'title' => "UCook",
            'css' => [
                        'templates/header.css',
                        'ask/ask.css',
            ],
            'js' => [
                        'component/fake.js',
            ]
        ];
        
        return  view('template/header', $data).
                view('component/general/banner').
                view('component/ask/time').
                view('template/footer');

    }

    public function ingredient()
    {
        $params = $this->request->getPost();
        if(isset($params['time'])){
            $this->session->set('time', $params['time']);
        }
        
        // $this->askModel->callOpenIA();
        
        $data = [
            'title' => "UCook",
            'user' => $this->session->get('time'),
            'css' => [
                        'templates/header.css',
                        'ask/ask.css',
            ],
            'js' => [
                        'component/fake.js',
            ]
        ];
        
        return  view('template/header', $data).
                view('component/general/banner').
                view('component/ask/ask').
                view('template/footer');

    }


}
