
const API_KEY = "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";

//Données factices
const mockup = "*Omelette tomate, jambon et oignon*$1- Découpez le jambon et l'oignon en petits cubes. $$2- Faites chauffer une poêle avec un filet d'huile d'olive. $$3- Ajoutez le jambon et l'oignon à la poêle et faites-les revenir quelques minutes. $$4- Ajoutez les tomates et mélangez le tout. $$5- Battez deux oeufs et versez-les sur le";
const mockup2 = "*Tartines Jambon/Tomate/Patate/Oignon*\n\n$1- Couper l'oignon et le faire revenir avec une noisette de beurre.\n$$2 - Eplucher et couper la patate en dés et la faire revenir avec l'oignon.\n$3 - Ajouter les tomates épluchées et coupées en dés.\n$4 - Couper le jambon en petits morceaux et l'ajouter à la préparation.\n$5 - Répartir tous les ingrédients sur les tartines et passer un court";
const mockup3 = " \n\n*Gratin de Jambon et Pomme de Terre aux Tomates et Oignons* \n \n$1.$ Épluchez et coupez les pommes de terre en rondelles. \n$2.$ Coupez les tomates et l'oignon en tranches. \n$3.$ Dans un plat à gratin, couchez les pommes de terre, le jambon coupé en morceaux, les tomates et l'oignon tranchés. \n$4.$ Salez, poivrez et arrosez d'huile d'olive. \n$5.$M";

async function generateText(){
    const result = await fetch("https://api.openai.com/v1/completions", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${API_KEY}`,
        },
        body: JSON.stringify({
            prompt: `donne moi une recette de cuisine de niveau ${settings.level} à réaliser en 5 étape maximum et en ${settings.time} minutes maximum. la recette ne doit comporter que ces ingrédients : ${settings.produit.toString()} je ne veux que les étapes de préparations de la recette. chaque étape de préparation doit être placée en deux caractere $ .rédige le titre de la recette et écris le entre deux caractere * . le titre ne doit pas être entre deux characters $`,
            max_tokens: 120,
            temperature: 0.1,
            model: "text-davinci-003",
        }),
    })
    .then((response) => response.json())
    .then((json) => {
        let text = json.choices[0].text;
        return text;
    })

    // const result = mockup;
    
    console.log(result);
    
    //On récupère le titre
    let regex = /\*(.*?)\*/;
    let titre = result.match(regex);
    titre = titre[1];
    console.log(titre);

    //On efface le titre de la réponse pour ne garder que la recette
    let recipe = result.replace(regex, '');
    console.log(recipe);
    
    //On nettoie le texte de la recette de tous caractère superflu
    recipe = recipe.replace(/\n/g, '');
    console.log(recipe);

    //On segmente les différentes étapes de la recette et on les place dans un tableau
    let recipeArr = recipe.split('$');
    console.log(recipe);

    //On retire toutes les entrées vide du tableau
    let etapes = Array();
    recipeArr.filter(value => value != "").forEach(v => etapes.push(v));
    console.log(etapes);

    /* regex = /\$[^$]*\$/g;
    let etapes = result.match(regex);
    console.log(etapes); */
    
    let elementTitle = document.getElementById('recipe-title');
    let elementList = document.getElementById('recipe-list');
    
    elementTitle.innerHTML = titre;
    
    etapes.forEach(element => {
        //On retire tous les $ du texte
        element = element.replace(/\$/g, "");

        //On créer les éléments html
        let etape = document.createElement('li');
        etape.setAttribute('class', "list-group-item");
        etape.innerHTML = element;
        elementList.appendChild(etape);
    });
    
    return result;
}

function loadingOn(){
    /* <div class="spinner-border text-success" role="status">
        <span class="visually-hidden">Loading...</span>
    </div> */

    let spinnerDiv = document.createElement('div');
    spinnerDiv.setAttribute('id', 'spinner');
    spinnerDiv.setAttribute('class', 'spinner-border text-success');
    spinnerDiv.setAttribute('role', 'status');

    let spinnerSpan = document.createElement('span');
    spinnerSpan.setAttribute('class', 'visually-hidden');

    spinnerDiv.appendChild(spinnerSpan);

    let element = document.querySelector('#recipe-bg > .row');
    element.appendChild(spinnerDiv);

    let elementList = document.querySelectorAll('#recipe-card');
    elementList.forEach(element => {
        element.style.display = "none";
    });
}

function loadingOff(){
    let spinnerDiv = document.getElementById('spinner');
    spinnerDiv.remove();

    let elementList = document.querySelectorAll('#recipe-card');
    elementList.forEach(element => {
        element.style.display = "block";
    });
    
}

async function generate(){
    loadingOn();
    let text = await generateText();
    loadingOff();

    console.log(settings);
    console.log(settings.level);
    console.log(settings.time);

    let askBg = document.getElementById('recipe-bg');
    // askBg.innerText = text;
}

/* async function addToDB(data){
    const res = await fetch('script.php', {
        method: 'POST',
        body: JSON.stringify({
          text: data["text"],
          url: data["imgUrl"]
        }),
        headers: {
          'Content-Type': 'application/json'
        }
    }).then(resultat => resultat.json())

    return res;
} */


// console.log(level);

generate();
